#input file from john hopkins university
#download the shapefile from https://hub.arcgis.com/datasets/a21fdb46d23e4ef896f31475217cbb08_1 , then download shape file
import branca.colormap as cm
import folium, sys
import geopandas as gpd
import numpy as np
import pandas as pd
from folium.features import GeoJsonPopup
from folium.features import GeoJson
#sys.setrecursionlimit(3500)
#change the input file
corona_df = pd.read_csv("agg_data_2020-05-05.csv")
countries = gpd.read_file('world+regions.json')
#print (countries)
corona_df = corona_df.replace({'country' :
                      dict.fromkeys(['Taiwan',
                                     'Mainland China',
                                     'Hong Kong',
                                     'Macau'],
                                     'China')})
corona_df = corona_df.replace({'country' : 'America'},
                                'United States')
corona_df = corona_df.replace({'country' : 'United_Kingdom'},
                                'United Kingdom')
corona_df = corona_df.replace({'country' : 'Saudi_Arabia'},
                                'Saudi Arabia')
corona_df = corona_df.replace({'country' : 'Czechia'},
                                'Czech Republic')
corona_df = corona_df.replace({'country' : 'South_Africa'},
                                'South Africa')
corona_df = corona_df.replace({'country' : 'Ivory_Coast'},
                                'Ivory Coast')
corona_df = corona_df.replace({'country' : 'Dominican_Republic'},
                                'Dominican Republic')
corona_df = corona_df.replace({'country' : 'Burkina_Faso'},
                                'Burkina Faso')
corona_df = corona_df.replace({'country' : 'Sierra_Leone'},
                                'Sierra Leone')
corona_df = corona_df.replace({'country' : 'Burma'},
                                'Myanmar (Burma)')
corona_df = corona_df.replace({'country' : 'South_Korea'},
                                'South Korea')
corona_df = corona_df.replace({'country' : 'North_Korea'},
                                'North Korea')
corona_df = corona_df.replace({'country' : 'Papua_New_Guinea'},
                                'Papua New Guinea')
corona_df = corona_df.replace({'country' : 'Bosnia'},
                                'Bosnia and Herzegovina')
corona_df = corona_df.replace({'country' : 'Western_Sahara'},
                                'Western Sahara')
corona_df = corona_df.replace({'country' : 'Costa_Rica'},
                                'Costa Rica')
corona_df = corona_df.replace({'country' : 'El_Salvador'},
                                'El Salvador')
corona_df = corona_df.replace({'country' : 'Cabo_Verde'},
                                'Cape Verde')
corona_df = corona_df.replace({'country' : 'North_Macedonia'},
                                'North Macedonia')
corona_df = corona_df.replace({'country' : 'Sri_Lanka'},
                                'Sri Lanka')
corona_df = corona_df.replace({'country' : 'United_Arab_Emirates'},
                                'United Arab Emirates')
corona_df = corona_df.replace({'country' : 'Central_African_Republic'},
                                'Central African Republic')
corona_df = corona_df.replace({'country' : 'Vatican City'},
                                'Italy')
corona_df = corona_df.replace({'country' : 'San_Marino'},
                                'Italy')
countries = countries.replace({'CNTRY_NAME' : 'Byelarus'},
                               'Belarus')
countries = countries.replace({'CNTRY_NAME' : 'Macedonia'},
                               'North Macedonia')
countries = countries.replace({'CNTRY_NAME' : 'Tanzania, United Republic of'},
                               'Tanzania')
countries = countries.replace({'CNTRY_NAME' : '"Man'},
                               'Man')
countries = countries.replace({'CNTRY_NAME' : '"Gambia'},
                               'Gambia')
countries = countries.replace({'CNTRY_NAME' : 'Bahamas, The'},
                               'The_Bahamas')
countries = countries.replace({'CNTRY_NAME' : '"Gambia, The"'},
                               'Gambia')
countries = countries.rename(columns={'CNTRY_NAME': 'country'})

corona_df = corona_df[corona_df.confirmed != 0]
sorted_df = corona_df.sort_values(['country',
                     'date']).reset_index(drop=True)
sum_df = sorted_df.groupby(['country', 'date'], as_index=False).sum()

countries = countries.rename(columns={'COUNTRY': 'country'})
countries = countries.rename(columns={'name': 'country'})

print (sum_df,countries)
joined_df = sum_df.merge(countries, on='country')
print (joined_df)
joined_df['log_Confirmed'] = np.log10(joined_df['confirmed'])
joined_df['date_sec'] = pd.to_datetime(joined_df['date']).astype(int) / 10**9
joined_df['date_sec'] = joined_df['date_sec'].astype(int).astype(str)
#We can now select the columns needed for the map and discard the others:
joined_df = joined_df[['country', 'date_sec', 'log_Confirmed', 'geometry']]
#print(joined_df.loc[7908,['geometry']])
max_colour = max(joined_df['log_Confirmed'])
min_colour = min(joined_df['log_Confirmed'])
#define scale colour
cmap = cm.linear.YlOrRd_09.scale(min_colour, max_colour)
joined_df['colour'] = joined_df['log_Confirmed'].map(cmap)
joined_df.to_csv("irene.csv",sep=',')

country_list = joined_df['country'].unique().tolist()
print (country_list)
country_idx = range(len(country_list))
#create dict with all countries
style_dict = {}
for i in country_idx:
    country = country_list[i]
    result = joined_df[joined_df['country'] == country]
    print (result)
    inner_dict = {}
    for _, r in result.iterrows():
        inner_dict[r['date_sec']] = {'color': r['colour'], 'opacity': 0.9}
    style_dict[str(i)] = inner_dict
#select country and geometry coloumns
countries_df = joined_df[['country','geometry']]
countries_gdf = gpd.GeoDataFrame(countries_df)
countries_gdf = countries_gdf.drop_duplicates().reset_index()
from folium.plugins import TimeSliderChoropleth

slider_map = folium.Map(min_zoom=2, max_bounds=True,tiles='cartodbpositron')
#convert to json file
data=countries_gdf.to_json()
#create TimeSliderChoropleth for dynamic map depend on the time
_ = TimeSliderChoropleth(
    data=data,
    styledict=style_dict,
    ).add_to(slider_map)
#other layer for the popup country info
a=GeoJson(data, popup=(folium.GeoJsonPopup(fields=['country'])),
              style_function=lambda feature: {'fillColor': 'white',
        'color': 'black', 'fillOpacity': 0.1, 'weight': 0.5})
a.add_to(slider_map)
_ = cmap.add_to(slider_map)
cmap.caption = "Log of number of confirmed cases"
slider_map.save(outfile='TimeSliderChoropleth.html')
