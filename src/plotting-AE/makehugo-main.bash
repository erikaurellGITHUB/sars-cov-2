#!/bin/bash -x


outdir=~/Desktop/Hugo-main/
hugodir=HugoTheme/
URL=ae.scilifelab.se:/var/www/html/covid19.bioinfo.se/docroot/Corona/


for dir in $*
do
    #echo $dir
    basedir=${outdir}/
    #subdir=${base}-nations
    mkdir -p ${basedir}/content/photo/data
    rsync -arv ${hugodir}/ ${basedir}/
    index=${basedir}/content/photo/data/index.md
    cat >${index} <<EOF
---
title: Different data series for corona plots

resources:

EOF

    for i in ${dir}/*/
    do
	j=`basename $i`
	montage $i/*png ${basedir}/content/photo/data/$j.png
    cat >>${index} <<EOF
- src: ${j}.png
  name: Data from ${j}
  params:
    order: ${j}
    description: ${j}
    button_text: ${j}
    button_url: "https://covid19.bioinfo.se/Corona/${j}/"

EOF
    done

    cat >>${index} <<EOF
---
EOF

    pushd ${basedir}/
    #hugo new content/photo/${base}/index.md
    /snap/bin/hugo -D -b "https://covid19.bioinfo.se/Corona/"
    rsync -arv  -e "ssh -i ~/.ssh/id2_rsa"  public/ $URL/$base/
    #rsync -arv    public/ $URL/
    popd
done


