#!/usr/bin/env python3

from __future__ import print_function
import pandas as pd
import numpy as np
import argparse
from argparse import RawTextHelpFormatter
from dateutil.parser import parse
from datetime import datetime,date,time, timedelta
from dateutil import parser
#import pyarrow
import matplotlib.pyplot as plt
import matplotlib as mpl
import seaborn as sns

startdate=datetime.strptime("2020-03-01","%Y-%m-%d")
stopdate=datetime.strptime("2020-05-01","%Y-%m-%d")

p = argparse.ArgumentParser(description = 
                                     '- AE-analysis.py - Extract data from date range and create models -',
            formatter_class=RawTextHelpFormatter)
p.add_argument('-i','--input', required= True, help='Input')
ns = p.parse_args()

df=pd.read_csv(ns.input,sep=",")
#print(df.columns)

numdays=(stopdate-startdate).days
#print (numdays)
dates=[]
for i in range(numdays):
    dates+=[startdate+timedelta(i)]
countries=df.country.drop_duplicates()
fig, ax1 = plt.subplots(figsize=(16,10))
lw=0.2
for country in countries:
    lw+=0.5
    temp_df=df.loc[df.country==country]
    l=[]
    mu=temp_df.loc[temp_df.parameter=="mu"]["mean"].mean()
    l+=[temp_df.loc[temp_df.parameter=="lambda_0"]["mean"].mean()-mu]
    l+=[temp_df.loc[temp_df.parameter=="lambda_1"]["mean"].mean()-mu]
    l+=[temp_df.loc[temp_df.parameter=="lambda_2"]["mean"].mean()-mu]
    l+=[temp_df.loc[temp_df.parameter=="lambda_3"]["mean"].mean()-mu]
    date=[0]
    date+=[temp_df.loc[temp_df.parameter=="transient_begin_0"]["mean"].mean()-mu]
    date+=[temp_df.loc[temp_df.parameter=="transient_begin_1"]["mean"].mean()-mu]
    date+=[temp_df.loc[temp_df.parameter=="transient_begin_2"]["mean"].mean()-mu]
    date+=[numdays]
    x=np.arange(numdays)
    y=[]
    for i in np.arange(4):
        for j in np.arange(int(date[i]),int(date[i+1])):
            y+=[l[i]]
            #print (i,j,l[i])
    ax1.plot(x,y, label=country, linewidth =lw)
    xticks=np.arange(0,len(y),7)
    
ax1.set_xticklabels(dates[0::7],rotation=45)
ax1.set_xticks(xticks)
#ax1.legend(loc='lower left', frameon=False, markerscale=2)
ax1.legend()
ax1.grid()
plt.gcf().subplots_adjust(bottom=0.25)
plt.tight_layout()

fig.savefig("lambda.png", format = 'png', dpi=300)
plt.close()




